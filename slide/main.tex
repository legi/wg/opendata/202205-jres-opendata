\documentclass[11pt,aspectratio=169]{beamer}

%\usepackage{beamerarticle}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{pgf}
  \usepackage{hyperref}
  \setjobnamebeamerversion{beamerexample2.beamer}
}

\mode<presentation>
{
%  \usetheme{Dresden}
  \usetheme{Boadilla} % Warsaw
%  \usetheme{Montpellier}
%  \usetheme{Berkeley}
  \usecolortheme{wolverine}
  \setbeamercovered{transparent}
}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{listings}
\usepackage{eurosym}
\usepackage{siunitx}
\usepackage{moresize}

\newlength{\myoneco}
\setlength{\myoneco}{0.5\textwidth}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[20 ans de science ouverte au LEGI]{\textsc{20 ans de science ouverte au laboratoire LEGI~:}}
\subtitle{Quels succès~? Quelles difficultés~? Quels enseignements~? \\[0.8ex] Un tremplin vers le futur}
\author[\textbf{Cyrille - Gabriel - Julien - Joël}]{Cyrille Bonamy (IR-CNRS), Gabriel Moreau (IR-CNRS),\\ Julien Chauchat (MCF-GINP), Joël Sommeria (DR-CNRS)}
\date[18 mai 2022 / Marseille]{18 mai 2022 / Marseille}

\institute[\textsc{\textbf{LEGI}}]{
  Laboratory \textsc{LEGI} - CNRS / UGA / Grenoble-INP - France}

\titlegraphic{\href{http://www.cnrs.fr/}{\includegraphics[height=1.2cm]{logo-cnrs}}\hspace*{0.8cm}\href{http://www.univ-grenoble-alpes.fr/}{\includegraphics[height=1cm]{logo-uga}}\hspace*{1.4cm}\href{http://www.legi.grenoble-inp.fr/}{\includegraphics[height=0.9cm]{logo-legi}}\hspace*{1.2cm}\href{https://www.jres.org/}{\includegraphics[height=1.2cm]{logo-jres}}}

\setbeamercolor{block title alerted}{use=structure,fg=black,bg=structure.fg!75!black}
\setbeamercolor{block body alerted}{parent=normal text,use=block title,fg=black,bg=block title.bg!10!bg}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame[plain]{\maketitle}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% LES 3 PILIERS SONT LES DONNEES LES LOGICIELS ET LES PUBLI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{La Science Ouverte}
	\begin{columns}
		\column[t]{4cm}
		\small
		\begin{block}{Données}
		\begin{itemize}
			\item Principes \textbf{FAIR}
			\item Plan de Gestion des Données : une obligation aujourd'hui
			\item Problématique de la durée de vie
		\end{itemize}
		\end{block}

		\column[t]{4cm}
		\small
		\begin{block}{Logiciels}
		\begin{itemize}
			\item \textbf{Libre}, OpenSource
			\item Nécessité d'associer logiciels et données
			\item FAIR4RS (RS = Research Software)
			\item Reproductibilité (computationnelle ?)
		\end{itemize}
		\end{block}

		\column[t]{4cm}
		\small
		\begin{block}{Publications}
		\begin{itemize}
			\item Green \textbf{OpenAccess} : pre-print et articles soumis
			\item Gold OpenAccess : facile mais onéreux~; Attention aux éditeurs qui en profitent~!
		\end{itemize}
		\end{block}
	\end{columns}
%	~\\[-5ex]
	\begin{block}{}
	\begin{itemize}
		\item Objectifs : transparence, reproductibilité, efficacité, confiance, sobriété, pérennité
		\item Pour qui ? \textbf{vous}, votre laboratoire, votre communauté, tout le monde
		\item Obstacles : le coût humain à court terme
	\end{itemize}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Les principes FAIR}
	\centering

	\href{https://www.ouvrirlascience.fr/fair-principles/}{\pgfimage[width=4cm]{fair}}
	\small
	\begin{block}{Findable}
		Les \textbf{métadonnées}, données et logiciels doivent pouvoir être trouvées tant par les humains que par les ordinateurs ; identifiant persistant et unique : Digital Object Identifier (doi)
	\end{block}
	\begin{block}{Accessible}
		Une fois trouvées, les utilisateurs doivent savoir comment y accéder (dépôts, protocoles\ldots)
	\end{block}
	\begin{block}{Interoperable}
		Les données doivent être publiées sous un \textbf{format standard} facilitant les échanges.
	\end{block}
	\begin{block}{Reusable}
		L’accès est défini au travers d’une \textbf{licence} qui précise les conditions d’usage.
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Ce qui s'est fait au LEGI~?}
	\begin{columns}
		\column[c]{6.5cm}
	\href{http://www.legi.cnrs.fr/web/spip.php?article757}{\pgfimage[width=6.5cm]{coriolis}}
		\column[c]{8cm}
		\small
	\begin{block}{Il y a 20 ans\ldots}
	\begin{itemize}
		\item Objectif : \textbf{échange} de données expérimentales (champs de vitesse, concentration\ldots)
		\item Coriolis : grand équipement accueillant des équipes du monde entier
		\item Exemple de données : des fichiers XML, PNG et \href{https://www.unidata.ucar.edu/software/netcdf/}{NetCDF}(quelques Go par expérience en 2000, des dizaines de To aujourd'hui)
		\item \textbf{Associé} à quelques logiciels libres (UVmat, puis fluidimage)
		\item Loin d'être parfait : organisation, reproductibilité, formats
	\end{itemize}
	\end{block}
	\end{columns}
%	\begin{alertblock}{}
%		Amélioration au fil du temps (organisation, métadonnées\ldots).
%	\end{alertblock}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Ce qui se fait actuellement au LEGI~?}
	\begin{columns}
		\column[c]{7cm}
		\small
	\begin{block}{Aujourd'hui}
	\begin{itemize}
		\item Application de ces principes bien plus large que CORIOLIS
		\item Idéalement : association données et logiciels avec publications
%		\item Plusieurs solutions de diffusion considérées au sein du laboratoire
		\item Quelques réticences, mais des débats
		\item Au mieux en fonction de nos moyens
	\end{itemize}
	\end{block}
%  \begin{textblock*}{57mm}[0,0](105mm,10mm)
  \begin{figure}
  \includegraphics[width=7cm]{vagues-de-sediments}
    \end{figure}
%  \end{textblock*}
		\column[c]{7cm}
  \begin{figure}
  \includegraphics[width=7cm]{cavit}
    \end{figure}
		\small
	\begin{block}{Une vraie dynamique}
	\begin{itemize}
		\item Un groupe de travail regroupant chercheurs et ingénieurs
		\item Une méthodologie rigoureuse
		\item Des financements sur projet (WP du projet européen Hydralab)
        	\item Une diffusion de notre savoir-faire et expérience : ANRs, Univ du Delaware
%		\item Mais des questions subsistent : durée de vie, modèle économique\ldots
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Focus concernant les données}
	\begin{block}{Plusieurs solutions pour le partage et la diffusion des données ouvertes}
	%There is more than one way to do it!
	\begin{itemize}
		\item Simplement sur \href{https://zenodo.org/}{Zenodo}
		\item Via un accès distant \href{https://www.opendap.org/}{OPeNDAP}
		\item Via le partage de scripts de post-traitement à distance : (\href{https://jupyter.org/}{Jupyter} Notebook + \href{https://www.opendap.org/}{OPeNDAP}) %j'aime pas trop ce titre
%		\item Créer une archive de données et la publier simplement sur \href{https://zenodo.org/}{Zenodo}
%		\item Créer un ensemble de données et le publier avec un accès distant \href{https://www.opendap.org/}{OPeNDAP}
%		\item Partager et utiliser des scripts libres en ligne pour post-traiter à distance un ensemble de données (\href{https://jupyter.org/}{Jupyter} Notebook + \href{https://www.opendap.org/}{OPeNDAP}) %j'aime pas trop ce titre
	\end{itemize}
	\end{block}

	\begin{block}{Quelques points d'intérêt}
	\begin{itemize}
		\item Importance d'associer des métadonnées
		\item Nécessité d'apposer une licence et quelques fichiers indispensables (README, AUTHOR\ldots)
		\item Choix de la solution en fonction des besoins et contraintes (Volume, Durée de vie)
	\end{itemize}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 1 : \href{https://zenodo.org/}{Zenodo} (CERN)}
	\begin{columns}
		\column[c]{8.5cm}
	\includegraphics[width=8.5cm]{fig1-zenodo-server.pdf}
		\column[c]{5.5cm}
	\small
	\begin{block}{}
		\textbf{Pour}
	\begin{itemize}\small
		\item Simple à utiliser
		\item ID unique par jeux de données (\texttt{doi})
	\end{itemize}
		\textbf{Contre}
	\begin{itemize}\small
		\item Limité à 50~Go
		\item Téléchargement intégral de l'archive
	\end{itemize}

		\textbf{Outils libres développés}
	\begin{itemize}\small
		\item \href{https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/project-meta}{project-meta}
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 1 : \href{https://zenodo.org/}{Zenodo} - Création d'un jeux de données libres}
	\begin{block}{\href{http://servforge.legi.grenoble-inp.fr/projects/soft-trokata/wiki/SoftWare/ProjectMeta}{\textbf{project-meta}} :
		boîte à outils libre en ligne de commande pour l'open-data}
	\begin{itemize}
		\item Déclaration des métadonnées dans un fichier au format \href{http://yaml.org/}{YAML}
		\item Basée sur les spécifications \textbf{\href{https://fr.wikipedia.org/wiki/Dublin_Core}{Dublin Core Metadata Initiative}}
			(\href{http://dublincore.org/}{DCMI})
		\item Format descriptif simple composé de quinze propriétés relatives :
		\begin{itemize}
			\item au contenu (titre, sujet, description, source, langue, relation, couverture)
			\item à la propriété intellectuelle (créateur, contributeur, éditeur, gestion des droits)
			\item à l'instanciation (date, type, format, identifiant de la ressource)
		\end{itemize}
		\item Extension au Dublin Core pour déclarer les fichiers à mettre en données ouvertes
		\item Génération automatique à la racine de l'archive des fichiers :
			\texttt{LICENSE.txt}, \texttt{README.txt}, \texttt{AUTHORS.txt}, \texttt{COPYRIGHT.txt}\ldots
	\end{itemize}
	\end{block}
	\begin{block}{} \small
		Ligne de commande : \\
		\tiny{\texttt{\href{http://servforge.legi.grenoble-inp.fr/projects/soft-trokata/wiki/SoftWare/ProjectMeta}{project-meta} make-zip}}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 2 : \href{https://www.opendap.org/}{OPeNDAP} - Data Access Protocol}
	\begin{columns}
		\column[c]{8.5cm}
	\includegraphics[width=8.5cm]{fig2-opendap-server.pdf}
		\column[c]{5.5cm}
	\small
	\begin{block}{}\small
		\textbf{Pour}
		\begin{itemize}\ssmall
			\item Pas de limite en taille
			\item Téléchargement partiel possible \\(ex. sous-ensemble d'un fichier \href{https://www.unidata.ucar.edu/software/netcdf/}{NetCDF})
			%\item Nice to prepare the Free Data archive
			\item Encourage l'utilisation de \textbf{formats ouverts et auto-documentés} (\href{https://www.unidata.ucar.edu/software/netcdf/}{NetCDF}, HDF5\ldots \href{https://www.opendap.org/}{OPeNDAP} backend)
		\end{itemize}

		\textbf{Contre}
		\begin{itemize}\ssmall
			\item Pas d'ID unique (\texttt{doi})
			\item Obligation d'avoir son propre serveur \href{https://www.opendap.org/}{OPeNDAP} (pas de serveur publique)
		\end{itemize}

		\textbf{Outils libres développés}
		\begin{itemize}\ssmall
			\item \href{https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/project-meta}{project-meta}
			\item \href{http://servforge.legi.grenoble-inp.fr/projects/soft-uvmat}{UVmat} : compatibilité \href{https://www.opendap.org/}{OPeNDAP}
		\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 2 : \href{https://www.opendap.org/}{OPeNDAP} -
		Visualisation distante (\href{https://en.wikipedia.org/wiki/Particle_image_velocimetry}{PIV})
		avec \href{http://servforge.legi.grenoble-inp.fr/projects/soft-uvmat}{UVmat}}
	\begin{columns}
		\column[c]{9cm}
	\href{http://servforge.legi.grenoble-inp.fr/projects/soft-uvmat}{\pgfimage[width=9cm]{capture-uvmat1}}
		\column[c]{4cm}
	\href{http://servforge.legi.grenoble-inp.fr/projects/soft-uvmat}{\pgfimage[width=4cm]{capture-uvmat2}}
	\end{columns}
	\begin{block}{\href{http://servforge.legi.grenoble-inp.fr/projects/soft-uvmat}{\textbf{UVmat}} :
		boîte à outils graphique libre \href{https://fr.wikipedia.org/wiki/MATLAB}{Matlab}
		pour la \href{https://en.wikipedia.org/wiki/Particle_image_velocimetry}{PIV}}
	\begin{itemize}
		\item Développeur principal \textbf{Joël Sommeria} (CNRS LEGI)
		\item \textbf{100\% compatible \href{https://www.opendap.org/}{OPeNDAP}}
		\item \small Traitement local sur un petit sous ensemble de données distantes \href{https://en.wikipedia.org/wiki/Particle_image_velocimetry}{PIV} sans téléchargement complet de l'archive !
	\end{itemize}
	\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 3 : \href{https://jupyter.org/}{Jupyter} Notebook + \href{https://www.opendap.org/}{OPeNDAP}}
	\begin{columns}
		\column[c]{8.5cm}
	\includegraphics[width=8.5cm]{fig3-jupyter-server.pdf}
		\column[c]{5.5cm}
	\small
	\begin{block}{}\small
	\textbf{Pour}
	\begin{itemize}\ssmall
		\item Pas de limite en taille, téléchargement partiel\ldots
		\item Pas besoin d'installer de logiciel sur le terminal utilisateur
		\item Utilise un langage ouvert pour le post-traitement (\href{https://www.python.org/}{Python})
		\item Partage des scripts de post-traitement
	\end{itemize}

	\textbf{Contre}
	\begin{itemize}\ssmall
		\item Pas d'ID unique (\texttt{doi})
		\item Obligation d'avoir son propre serveur \href{https://www.opendap.org/}{OPeNDAP} (pas de serveur publique)
	\end{itemize}

	\textbf{Outils libres développés}
	\begin{itemize}\ssmall
		\item De nombreux notebooks \href{https://www.python.org/}{Python}
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Niveau 3 : \href{https://jupyter.org/}{Jupyter} Notebook + \href{https://www.opendap.org/}{OPeNDAP} - Traitements distants}
	\begin{columns}
		\column[c]{10cm}
	\href{https://github.com/CyrilleBonamy/notebook_opendata}{\pgfimage[width=9.5cm]{capture-jupyter-ncview1}}
	\begin{block}{}
	\tiny \url{https://mybinder.org/v2/gh/CyrilleBonamy/notebook\_opendata/HEAD?labpath=plot\_14CARR.ipynb}
	\end{block}
		\column[c]{4cm}
	%\begin{tabular}{cc}
	\href{https://github.com/CyrilleBonamy/notebook_opendata}{\pgfimage[width=3.5cm]{capture-jupyter-ncview2}}
	%\href{https://jupyter.org/}{Jupyter} Notebook & \texttt{ncview}
	%\end{tabular}
	\small
	\begin{block}{}
	\begin{itemize}\ssmall
		\item Tous les post-traitements sont effectués depuis un navigateur Web \\
		\item Pas de dépendance au terminal utilisateur
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{En matière de logiciels}
	\begin{columns}
		\column[c]{6.2cm}
		\includegraphics[width=6.2cm]{software-v1.pdf}
		\column[c]{8cm}
	\begin{block}{\emph{FAIR4RS autant que possible}}
	\begin{itemize}
		\item Gestion de version, intégration continue, documentation, licence
		\item Diffuser des version pérennes et identifiables via \href{https://zenodo.org/}{Zenodo} (identifiant unique : doi)
			et \href{https://www.softwareheritage.org/}{Software Heritage} (\href{https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html}{SWHID})
		\item Formats (standards) des fichiers d'entrée et sortie
		\item Reproductibilité (tests)
		\item Impacts environnementaux (CI, volume)
		\item Ne pas attendre~! Dès le début~!
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{En matière de publications}
	\begin{columns}
		\column[c]{9cm}
	\begin{block}{Publications : \emph{Ouverture}}
	\begin{itemize}
		\item Partager les «~données~» (\emph{a minima} les configurations numériques) et logiciels associés à la publication
		\item Fournir les scripts libres de post-traitement pour recréer les images
		\item Documenter~!
		\item Ne pas hésiter à ouvrir la publication dès que possible : Green OpenAccess
		\item Utiliser les outils de nos tutelles (\href{https://hal.archives-ouvertes.fr/}{Hal}, forge\ldots)
	\end{itemize}
	\end{block}
	\column[c]{5cm}
		\center
		\includegraphics[width=4cm]{publications.pdf}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Difficultés}
	\begin{columns}
		\column[c]{8cm}
	\begin{block}{}
	\begin{itemize}
		\item Certaines craintes : critiques, mauvaise utilisation ou à des fins négatives, nécessité de fournir du support\ldots
		\item La phrase clamée par le CNRS :
			\begin{center}
				«~\emph{accessible autant que possible, \qquad ~ \\ \qquad et fermé autant que nécessaire}~» \qquad ~
			\end{center}
		\item Coût : humain (service aux utilisateurs, documentation), publications
		\item Licences d'origine parfois bloquantes
		\item Démarche trop peu valorisée
	\end{itemize}
	\end{block}
		\column[c]{6cm}
		\href{https://www.biorxiv.org/content/10.1101/143503v1.full.pdf}{\pgfimage[width=6cm]{reproductibility}}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Épilogue}
	\begin{columns}
		\column[c]{8cm}
	\begin{block}{Points saillants}
	\begin{itemize}
		\item $1^{\mbox{\tiny er}}$ bénéficiaire $\rightarrow$ initiateur
		\item Dès le montage du projet (DMP, SMP)
		\item Licences au plus tôt
		\item Apposer une durée de vie aux données
		\item Citer des données via un doi n'est pas naturel
		\item Coût humain à court terme, mais bénéfique à moyen terme
		\item Science ouverte et éco-responsabilité non opposées
	\end{itemize}
	\end{block}
	\column[c]{5cm}
	\begin{block}{Quelques liens intéressants}
	\begin{itemize}
		\item \href{https://rd-alliance.org/}{Initiative européenne RDA}
		\item \href{https://www.science-ouverte.cnrs.fr/}{Science Ouverte au CNRS}
		\item \href{https://www.science-ouverte.cnrs.fr/wp-content/uploads/2021/01/Plaquette-Plan-Donnees-Recherche-CNRS_nov2020.pdf}{Plan de données ouvertes du CNRS}
		\item \href{https://gricad.gricad-pages.univ-grenoble-alpes.fr/cellule-data-stewardship/web/}{Cellule data-stewardship Grenobloise}
		\item \href{https://www.library.manchester.ac.uk/using-the-library/staff/research/open-research/access/funding/}{\emph{Open Acess Funding} à Manchester}
		\item \href{https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/project-meta}{Project-meta}
	\end{itemize}
	\end{block}
	\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
	\frametitle{Licence}
	\begin{center}
		\textbf{Merci de votre attention !} \\[4ex]
		Cette présentation est sous : \href{http://artlibre.org/}{\textsc{Licence Art Libre}} \\[2.5ex]
		\href{http://artlibre.org/}{\texttt{http://artlibre.org/}} \\[2.5ex]
	\begin{columns}
		\column[b]{2cm}
		\href{http://artlibre.org/}{\pgfimage[height=10ex]{logo-licence-art-libre}}
		\column[b]{2cm}
		\href{https://gricad-gitlab.univ-grenoble-alpes.fr/legi/wg/opendata/202205-jres-opendata}{\pgfimage[height=10ex]{icon-download-code}}
	\end{columns}
	\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}


Zenodo : Simple, Data have a uique ID

Limitation (upload complet, grosse archive, phrase de mise au point, data are not always in a portable format : CSV, HDF5, \href{https://www.unidata.ucar.edu/software/netcdf/}{NetCDF}.

Accès aux données depuis son poste via le protocol DAP
 - \href{https://www.python.org/}{Python}
 - \href{https://fr.wikipedia.org/wiki/MATLAB}{Matlab} (exemple UVmat)

Accès aux données depuis un service web \href{https://jupyter.org/}{Jupyter} (\href{https://www.python.org/}{Python})


Concernant mon graphe, ptet une box à droite telle que (ptet à réordonner) :

Titre : Points de vigilance :
 - FAIR4RS
 - Impacts environnementaux (CI, volumétrie)
 - Reproductibilité
 - Licence
 - Formats des fichiers de sortie
 - Ne pas attendre! dès le début!

Mettre quelque part la vision globale logiciel-data-publication

Mettre le dépot Git en libre accès une fois au JRES.

Volumétrie n'est pas le bon mot... Volume


Piste pour des questions eventuelles pour les chairmans (si besoin) : 
1) Vous dites que Science ouverte et éco-responsabilité ne sont pas opposées. Pouvez vous en dire plus?
2) Vous dites que le premier bénéficiaire de la Science Ouverte est la personne initiant la démarche. Pouvez vous nous expliquer pourquoi? 
