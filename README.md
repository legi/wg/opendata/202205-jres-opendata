# JRES 2022 - OpenData

## Astuce pour l'article

Pour passer du pad au format odt en poussant les styles
(Voir https://help.libreoffice.org/4.4/Writer/Using_Styles_From_Another_Document_or_Template/fr).

 * Styles -> Charger les Styles
 * On coche tout : Textes, Numération, Écraser...
 * À partir d'un fichier (choisir alors le modèle des JRES)
 * Et voila

Il faudra alors mettre dans le fichier final
 * Le bon style pour les adresses des auteurs
 * Les images
 * Bien intégrer les références bibliographiques

On poursuit alors avec un document au format fodt qui est de l'odt à plat (flat),
ce qui est mieux pour le versionner avec Git.
L'intégration continue et le Makefile génère l'odt et le pdf qui vont bien.

## Artifacts article et présentation

Sur la page du projet, vous pouvez télécharger le PDF et/ou le ODT dans
`downloads artifacts -> create-pdf`.
Ces deux fichiers fabriqués par intégration continue,
donc mis à jour à chaque changement dans le repository.
Une archive [ZIP](https://gricad-gitlab.univ-grenoble-alpes.fr/legi/wg/opendata/202205-jres-opendata/-/jobs/artifacts/master/download?job=create-pdf)
directement accessible contient ces deux fichiers.

https://gricad-gitlab.univ-grenoble-alpes.fr/legi/wg/opendata/202205-jres-opendata/-/jobs/artifacts/master/download?job=create-pdf
